#!/bin/bash

# Script to generate the outline for the codedev part of the REMIND Meeting.
# Meant to be executed from a git parent directory with all repositories as
# subfolders using their default names (as given by git clone).
# To ensure full functionality this script is best executed on the cluster in
#     /p/projects/remind/rm-codedev-generator
# Give as an argument the date after which the code changes should be listed:
#     bash generator.sh "21.09.2023 00:00"

# File for output results
out="remind-meeting.out"

# Print date of last generation
if [ -f $out ]; then
  lastDate=$(date -r $out)
  echo "For information: last generation of the list was on $lastDate. If relevant, you may generate the new list with:"
  echo "    bash generator.sh '$lastDate'"
  echo
fi

# Interrupt if no starting date is provided as an argument
if [ -z "$1" ]; then
  echo 'Please provide a starting date, for example:'
  echo '    bash generator.sh "21.09.2023 00:00"'
  exit 0
fi

# Date after which the code changes should be listed
startDate="$1"

echo "Generating list of commits starting $startDate"
echo
echo "#### Code Development since _${startDate}_" >"$out"

## Part 1: REMIND model
echo "- REMIND model"
echo "- REMIND" >>"$out"

cd REMIND
git checkout --quiet develop
git pull --quiet https://github.com/remindmodel/remind.git develop
git log --merges --first-parent develop --after="$startDate" \
  --format="%aN%x09%ad%x09%s%x09%b" --date=format:"%a %b %d" |
  sort -bt $'\t' -k 1,1 -k 2.5M -k 2.9n |
  sed "s|\(.*\)\t\(.*\)\tMerge pull request #\([0-9]\+\).*\t\(.*\)|  - [#\3](https://github.com/remindmodel/remind/pull/\3) _\1 (\2)_ \4|" >>"../$out"
cd ..

## Part 2: R packages (merges)
# Lists all *merges* that have been performed into master.
echo "- R packages (merges)"
echo "- R packages" >>"$out"

repos=(mrcommons mrremind mrdrivers mrindustry remind2 edgeTransport mrtransport reporttransport piamUtils quitte mip magclass lucode2 gms modelstats piamInterfaces piamMappings piamValidation)

for i in "${repos[@]}"; do
  echo "    - $i"
  echo "  - $i" >>$out
  if [ ! -d "$i" ]; then
    # Clone if $i does not exist.
    echo "Cloning ${i}..."
    git clone "https://github.com/pik-piam/$i.git"
  fi
  cd $i
  # check out the default branch of origin, whatever it is called
  branch=$(git symbolic-ref refs/remotes/origin/HEAD | sed 's|refs/remotes/origin/||')
  git checkout --quiet "$branch"
  git pull --quiet "https://github.com/pik-piam/$i.git" "$branch"
  git log --merges --first-parent "$branch" --after="$startDate" \
    --format="%aN%x09%ad%x09%s%x09%b" --date=format:"%a %b %d" |
    sort -bt $'\t' -k 1,1 -k 2.5M -k 2.9n |
    egrep -v '\[pre-commit\.ci\]' |
    sed "s|\(.*\)\t\(.*\)\tMerge pull request #\([0-9]\+\).*\t\(.*\)|    - [#\3](https://github.com/pik-piam/$i/pull/\3) _\1 (\2)_ \4|" >>"../$out"
  cd ..
done

## Part 3: Other (commits)
# Lists all *commits* that have been pushed to master.
# This is only useful if changes have been pushed directly to master
# without pull request and subsequent merge (not the standard way).
echo "- Other (commits)"
echo "- Other (commits)" >>"$out"

repos=(tools) # if you know of repos where direct commits have been performed fill them in here otherwise leave blank

for i in "${repos[@]}"; do
  echo "    - $i"
  echo "  - $i (commits)" >>$out
  if [ ! -d "$i" ]; then
    # Clone if $i does not exist.
    echo "Cloning ${i}..."
    git clone "https://github.com/pik-piam/$i.git"
  fi
  cd $i
  git checkout --quiet master
  git pull --quiet
  git log master --after="$startDate" --pretty=format:"    - [%h](https://github.com/pik-piam/$i/commit/%H) _%aN (%ad)_ %s" --date=format:"%a %b %d" | tac >>../$out
  cd ..
done

echo "    - rm-codedev-generator"
echo "  - rm-codedev-generator" >>$out
git log master --after="$startDate" --pretty=format:"    - [%h](https://gitlab.pik-potsdam.de/REMIND/rm-codedev-generator/-/commit/%H) _%aN (%ad)_ %s" --date=format:"%a %b %d" | tac >>$out

echo
echo "List completed, see $out"
