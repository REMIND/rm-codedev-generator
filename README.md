# rm-codedev-generator

A script to generate a list of code developments for the REMIND meeting from `git log`.
It descends into repository subfolders, updates them and generates a copy-paste-able list of PR titles for the REMIND meeting minutes file.

## Usage

Execute from a git parent directory with all repositories as subfolders using their default names (as given by git clone).
On the cluster, use the folder `/p/projects/remind/rm-codedev-generator`.

### Parameters
- `out`: filename for the output. Defined in the script as "remind-meeting.out" 
- `startDate`: date from which PRs are collected, for instance with format "21.09.2023 00:00"

### Examples 
- `$ bash generator.sh "21.09.2023 00:00"` generates the list of PRs from 21.09.2023 onward

## Known Issues

- every now and then, **line-breaks are missing**. Please correct upon pasting/fix code.
- author is the author of the merge commit on the *first-parent*, i.e., develop or master, but should be the author of the *second-parent*, i.e., the author of the last commit in the branch to-be-merged.
- non-default repository paths will stop the script from working.
- non-existing repository paths will stop the script from working.
